use Test::More;
BEGIN {
    $INC{"File/ArchivableFormats.pm"} = 1;
}

use Zaaksysteem::Tools::File qw(sanitize_filename);

my $sane = sanitize_filename("sane.odt");
is($sane, 'sane.odt', "Sane filename stays unchanged");

my $insane = sanitize_filename(qq{disallowed<>"&|?*:\t.odt});
is($insane, 'disallowed________.odt', "Filename with disallowed characters gets fixed");

for my $winfile (qw(lpt2 com4 prn nul con)) {
    my $windows = sanitize_filename($winfile);
    is($windows, "${winfile}_", "Windows special device '$winfile' gets an underscore appended");
}

my $fullpath = sanitize_filename('C:\Users\Default\Desktop\Hidden\Image.jpg');
is($fullpath, "Image.jpg", "Full paths get truncated to the last element");

done_testing();
